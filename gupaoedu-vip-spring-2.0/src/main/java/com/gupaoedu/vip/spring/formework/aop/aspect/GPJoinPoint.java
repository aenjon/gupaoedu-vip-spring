package com.gupaoedu.vip.spring.formework.aop.aspect;

import java.lang.reflect.Method;

/**
 * 回调连接点，通过它可以获得被代理的业务方法的所有信息
 * Created by Tom on 2019/4/15.
 */
public interface GPJoinPoint {

    Object getThis();

    Object[] getArguments();

    Method getMethod();

    //在JointPoint中添加自定义属性
    void setUserAttribute(String key, Object value);

    //从已添加得自定义属性汇总获取一个属性值
    Object getUserAttribute(String key);
}
