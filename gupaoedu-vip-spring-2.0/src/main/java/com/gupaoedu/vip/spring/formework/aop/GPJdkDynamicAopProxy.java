package com.gupaoedu.vip.spring.formework.aop;

import com.gupaoedu.vip.spring.formework.aop.intercept.GPMethodInvocation;
import com.gupaoedu.vip.spring.formework.aop.support.GPAdvisedSupport;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;

/**
 * 使用和JDK Proxy API生成代理类
 * Created by Tom on 2019/4/14.
 */
public class GPJdkDynamicAopProxy implements GPAopProxy, InvocationHandler {

    private final GPAdvisedSupport advised;

    public GPJdkDynamicAopProxy(GPAdvisedSupport config) {
        this.advised = config;
    }

    //把原生的对象传进来
    @Override
    public Object getProxy() {
        return getProxy(this.advised.getTargetClass().getClassLoader());
    }

    @Override
    public Object getProxy(ClassLoader classLoader) {
        return Proxy.newProxyInstance(classLoader, this.advised.getTargetClass().getInterfaces(), this);
    }

    //invoke()方法是执行代理的关键入口
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //将每一个JoinPoint也就是被代理的业务方法（Method）封装成一个拦截器，组合成一个拦截器链
        List<Object> interceptorsAndDynamicMethodMatchers = this.advised.getInterceptorsAndDynamicInterceptionAdvice(
                method, this.advised.getTargetClass());
        //交给拦截器链MethodInvocation的proceed()方法执行
        GPMethodInvocation invocation = new GPMethodInvocation(proxy, this.advised.getTarget(), method, args,
                                                               this.advised.getTargetClass(),
                                                               interceptorsAndDynamicMethodMatchers);
        return invocation.proceed();
    }
}
