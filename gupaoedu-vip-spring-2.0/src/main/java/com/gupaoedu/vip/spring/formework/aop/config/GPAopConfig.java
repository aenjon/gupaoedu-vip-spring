package com.gupaoedu.vip.spring.formework.aop.config;

import lombok.Data;

/**
 * AOP配置封装
 * 所有成员变量与properties文件中属性一一对应
 * Created by Tom on 2019/4/15.
 */
@Data
public class GPAopConfig {
    /**
     * 切面表达式
     */
    private String pointCut;
    /**
     * 前置通知方法名
     */
    private String aspectBefore;
    /**
     * 后置通知方法名
     */
    private String aspectAfter;
    /**
     * 要植入的切面类
     */
    private String aspectClass;
    /**
     * 异常通知方法名
     */
    private String aspectAfterThrow;
    /**
     * 需要通知的异常类型
     */
    private String aspectAfterThrowingName;
}
