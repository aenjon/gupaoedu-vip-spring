package com.gupaoedu.vip.spring.formework.webmvc.servlet;

import java.util.Locale;
import java.io.File;

/**
 * 设计这个累的主要目的是：
 * 1.将一个静态文件变为一个动态文件
 * 2.根据用户传送不同的参数，产生不同的结果
 * 最终输出字符串，交给Response输出
 * Created by Tom on 2019/4/13.
 */
public class GPViewResolver {

    private final String DEFAULT_TEMPLATE_SUFFX = ".html";

    private File templateRootDir;
//    private String viewName;

    public GPViewResolver(String templateRoot) {
        String templateRootPath = this.getClass().getClassLoader().getResource(templateRoot).getFile();
        templateRootDir = new File(templateRootPath);
    }

    public GPView resolveViewName(String viewName, Locale locale) throws Exception{
        if(null == viewName || "".equals(viewName.trim())){return null;}
        viewName = viewName.endsWith(DEFAULT_TEMPLATE_SUFFX) ? viewName : (viewName + DEFAULT_TEMPLATE_SUFFX);
        File templateFile = new File((templateRootDir.getPath() + "/" + viewName).replaceAll("/+","/"));
        return new GPView(templateFile);
    }

//    public String getViewName() {
//        return viewName;
//    }
}
