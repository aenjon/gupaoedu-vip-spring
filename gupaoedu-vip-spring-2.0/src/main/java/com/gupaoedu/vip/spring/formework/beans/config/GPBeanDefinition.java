package com.gupaoedu.vip.spring.formework.beans.config;

import lombok.Data;

/**
 * 用来存储配置文件中的信息
 * 相当于保存在内存中的配置
 * Created by Tom.
 */
@Data
public class GPBeanDefinition {
    /**
     * 原生Bean的全类名
     */
    private String beanClassName;
    /**
     * 标记是否延时加载
     */
    private boolean lazyInit = false;
    /**
     * 保存beanName，在IoC容器中存储的key
     */
    private String factoryBeanName;
    private boolean isSingleton = true;
}
