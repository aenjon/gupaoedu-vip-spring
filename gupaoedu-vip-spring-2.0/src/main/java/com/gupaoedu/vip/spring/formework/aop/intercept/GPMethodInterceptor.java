package com.gupaoedu.vip.spring.formework.aop.intercept;

/**
 * 方法拦截器顶层接口
 * Created by Tom on 2019/4/14.
 */
public interface GPMethodInterceptor {
    Object invoke(GPMethodInvocation invocation) throws Throwable;
}
