package com.gupaoedu.vip.spring.formework.aop;

/**
 * 代理工厂的顶级接口，提供获取代理对象的顶级入口
 * Created by Tom.
 */
public interface GPAopProxy {
    //获得一个代理对象
    Object getProxy();
    //通过自定义类加载器获得一个代理对象
    Object getProxy(ClassLoader classLoader);
}
